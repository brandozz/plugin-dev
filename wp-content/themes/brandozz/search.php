<?php get_header(); ?>

	<div class="container">
		
		<div class="row">
			
			<div class="col-md-8">
		
				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						
						<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'brandozz' ), get_search_query() ); ?></h1>
					</header>

					<?php
					
					while ( have_posts() ) : the_post(); ?>
						
						<article>
							
							<header>
								
								<h2>
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h2>
							
								<small><?php the_time('F jS, Y') ?>  <?php the_author() ?></small>
           			 		
							</header>
							
							<section>
		            			
								<?php the_content('Read More &raquo;'); ?>
							
							</section>
						
						</article>
						
				<?php endwhile;

					// Previous/next page navigation.
					the_posts_pagination( array(
						'prev_text'          => __( '&larr; Previous', 'brandozz' ),
						'next_text'          => __( 'Next &rarr;', 'brandozz' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'brandozz' ) . ' </span>',
					) );

				// If no content, include the "No posts found" template.
				else : ?>
		            
					<article id="post-not-found">
		            	<header>
							
		                	<h1><?php _e("No Posts Yet", "brandozz"); ?></h1>
							
		                </header>
						
		                <section class="post_content">
							
		                	<p><?php _e("Sorry, What you were looking for is not here.", "brandozz"); ?></p>
							
		                </section>
			
					</article>

				<?php endif; ?>
			
			</div>
			
			<div class="col-md-4">
				
				<?php get_template_part('sidebar')?>
			
			</div>
		
		</div>

	</div>

	
<?php get_footer(); ?>