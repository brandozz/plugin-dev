<?php get_header(); ?>

<div class="container">
	<!-- Start the WordPress Loop -->
	<?php if (have_posts() ) : ?> 
	
		<?php while (have_posts()) : the_post(); ?>
	
			<article id="post-<?php the_ID(); ?>" class="entry">
		
				<header>
				
					<h1><?php the_title(); ?></h1>
				
				</header>
				
				<section>
		
					<?php the_content(); ?>
				
				</section>
			
			</article>
		
		<? endwhile; ?>
		
	<?php endif; ?>

</div>

<?php get_footer(); ?>