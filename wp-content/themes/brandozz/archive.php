<?php get_header(); ?>

<div class="container">
	
	<div class="row">
		
		<div class="col-md-8">
			
			<?php if (is_category()) { ?>
			
				<h1 class="archive_title h2">
               	 	<span><?php _e("Post Category:", "brandozz"); ?></span> <?php single_cat_title(); ?>
            	</h1>
			
            <?php } elseif (is_tag()) { ?>
            	<h1 class="archive_title h2">
                	<span><?php _e("Posts Tagged:", "brandozz"); ?></span> <?php single_tag_title(); ?>
               	</h1>
            <?php } elseif (is_author()) { ?>
              	<h1 class="archive_title h2">
               		<span><?php _e("Posts By:", "brandozz"); ?></span> <?php get_the_author_meta('display_name'); ?>
              	</h1>
            <?php } elseif (is_day()) { ?>
                <h1 class="archive_title h2">
                	<span><?php _e("Daily Archives:", "brandozz"); ?></span> <?php the_time('l, F j, Y'); ?>
                </h1>
            <?php } elseif (is_month()) { ?>
                <h1 class="archive_title h2">
                	<span><?php _e("Monthly Archives:", "brandozz"); ?>:</span> <?php the_time('F Y'); ?>
                </h1>
            <?php } elseif (is_year()) { ?>
                <h1 class="archive_title h2">
                    <span><?php _e("Yearly Archives:", "brandozz"); ?>:</span> <?php the_time('Y'); ?>
                </h1>
            <?php } ?>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<article>
				
					<header>
				
						<h2>
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h2>
		
						<small>Posted on <?php the_time('F jS, Y') ?> by <?php the_author() ?></small>
				
					</header>
           
            		<section>
				
						<?php the_content('Read More &raquo;'); ?>
				
					</section>
				
				</article>                                                                                       
            
			<?php endwhile; ?>       
           
			<?php
			
			// pagination
			the_posts_pagination( array(
				'prev_text'          => __( '&larr; Previous', 'brandozz' ),
				'next_text'          => __( 'Next &rarr;', 'brandozz' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'brandozz' ) . ' </span>',
			) );
           
            // If no content, include the "No posts found" template.
			else : ?>
			
            <article id="post-not-found">
            	
				<header>
                	
					<h1><?php _e("No Posts Yet", "brandozz"); ?></h1>
					
                </header>
				
                <section>
					
                	<p><?php _e("Sorry, What you were looking for is not here.", "brandozz"); ?></p>
					
                </section>
	
			</article>
			
		<?php endif; ?>
			
		</div>
		
		<div class="col-md-4">
			
			<?php get_template_part('sidebar'); ?>
		
		</div>
	
	</div>
	
</div>
	
<?php get_footer(); ?>