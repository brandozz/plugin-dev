<?php
/*
Template Name: Page Sidebar
*/
?>

<?php get_header(); ?>

<div class="container">
	
	<div class="row">
		
		<div class="col-md-8">
	
			<!-- Start the WordPress Loop -->
			<?php if (have_posts() ) : ?> 
	
				<?php while (have_posts()) : the_post(); ?>
	
					<article id="post-<?php the_ID(); ?>" class="entry">
						
						<header>
		
							<h1><?php the_title(); ?></h1>
						
						</header>
						
						<section>
		
							<?php the_content(); ?>
							
						</section>
			
					</article>
		
				<? endwhile; ?>
		
			<?php endif; ?>
		
		</div>
		
		<div class="col-md-4">
			
			<?php get_template_part('sidebar')?>
			
		</div>
	
	</div>

</div>

<?php get_footer(); ?>