<!DOCTYPE html>
<html lang="en">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title(''); ?></title>
		<?php do_action('wp_head'); ?>
	</head>
	<body>
		
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    	<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>">
						<?php bloginfo('name'); ?>
					</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					
					<!--<ul class="nav navbar-nav">
						
						<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
						<li><a href="#">Link</a></li>
				
					</ul>-->
					
					<?php
						if ( has_nav_menu( 'primary-menu' ) ) { /* if menu location 'primary-menu' exists then use custom menu */
					      wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => false, 'menu_class' => 'nav navbar-nav') ); 
					}
					?>
					
				</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>