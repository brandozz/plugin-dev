<form action="<?php echo esc_url( home_url() ); ?>" method="get" id="searchform">
	<fieldset>
		<input type="text" name="s" id="search" placeholder="Search <?php bloginfo('name'); ?>" value="<?php the_search_query(); ?>" />
		<!--<input type="image" alt="Search" src="<?php //bloginfo( 'template_url' ); ?>/images/search.png" />-->
	</fieldset>
</form>