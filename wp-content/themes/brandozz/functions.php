<?php
// Create Primary nav to use Wordpress dashboard
function register_menu() {
	register_nav_menu('primary-menu', __('Primary Menu'));
}
add_action('init', 'register_menu');

// Register Sidebar
function brandozz_sidebar() {

	$args = array(
		'id'            => 'sidebar-1',
		'name'          => __( 'sidebar', 'text_domain' ),
		'class'         => 'sidebar',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
	);
	register_sidebar( $args );

}
// Hook into the 'widgets_init' action
add_action( 'widgets_init', 'brandozz_sidebar' );

// Register Blog Sidebar
function brandozz_sidebar_blog() {
	$args = array(
		'id'            => 'sidebar-blog',
		'name'          => __( 'sidebar blog', 'text_domain' ),
		'description'   => __( 'Sidebar for the blog', 'text_domain' ),
		'class'         => 'sidebar-blog',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
	);
	register_sidebar( $args );

}
// Hook into the 'widgets_init' action
add_action( 'widgets_init', 'brandozz_sidebar_blog' );

// get stylesheets and scripts
if (!function_exists('brandozz_styles')) {
	function brandozz_styles() {
		wp_register_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.css', array(), '1.0' );

		wp_enqueue_style('bootstrap');
	}
}
add_action('wp_enqueue_scripts', 'brandozz_styles');

if (!function_exists( 'brandozz_scripts' )) {
	function brandozz_scripts() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', false, '2.1.3', true);
		wp_register_script('bootstrap_js', get_template_directory_uri() . '/javascripts/bootstrap.js', 'jquery', '1.0', true);
		wp_register_script('scripts', get_template_directory_uri() . '/javascripts/scripts.js', 'jquery', '1.0', true);

		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap_js');
		wp_enqueue_script('scripts');
	}
}
add_action('wp_enqueue_scripts', 'brandozz_scripts');
// Covering Homepage

add_filter('wp_title', 'baw_hack_wp_title_for_home');
function baw_hack_wp_title_for_home($title) {
	if (empty($title) && (is_home() || is_front_page())) {
		return __('Home', 'theme_domain') . ' | ' . get_bloginfo('description');
	}
	return $title;
}
