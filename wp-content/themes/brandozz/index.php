<?php get_header(); ?>

	<div class="container">
		
		<div class="row">
			
			<div class="col-md-8">
		
				<?php if (have_posts() ) : ?> 
		
					<?php while (have_posts()) : the_post(); ?>
	
						<article id="post-<?php the_ID(); ?>" class="entry">
					
							<header>
							
								<h2>
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h2>
					
								<small>Posted on <?php the_time('F jS, Y') ?> by <?php the_author() ?></small>
							
							</header>
							
							<section>
								
								<?php the_content('Read More &raquo;'); ?>
							
							</section>
						
						</article>
		
					<?php endwhile;
		
					// pagination
					the_posts_pagination( array(
						'prev_text'          => __( '&larr; Previous', 'brandozz' ),
						'next_text'          => __( 'Next &rarr;', 'brandozz' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'brandozz' ) . ' </span>',
					) );
		
					endif; ?>
			
			</div>
			
			<div class="col-md-4">
				
				<?php get_template_part('sidebar', 'blog') ?>
			
			</div>
		
		</div>

	</div>

	
<?php get_footer(); ?>